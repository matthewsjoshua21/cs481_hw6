﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Globalization;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;
using Plugin.Connectivity;


namespace CS481_HW6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        //function to handle event 
        async void Input_OnCompleted(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                //create client object and declare header with api key
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization",
                    "Token " + "8953a5cb3a7dbc50eef945a49a467dc6a2d0a97d");
                //create uri with concatenated request from user
                var uri = new Uri(
                    string.Format($"https://owlbot.info/api/v4/dictionary/" + $"{Input.Text}"));

                HttpRequestMessage request;
                request = new HttpRequestMessage {Method = HttpMethod.Get, RequestUri = uri};

                HttpResponseMessage response = await client.SendAsync(request);
                //verify request
                if (response.IsSuccessStatusCode)
                {
                    UserDialogs.Instance.Toast("Success!", TimeSpan.MaxValue);
                }
                else
                {
                    Console.WriteLine(response.StatusCode.ToString());
                }

                //read contents of search into a json string
                var jsonContent = await response.Content.ReadAsStringAsync();

                //create dictionary object here and handle errors
                try
                {
                    Dictionary define = Dictionary.FromJson(jsonContent);
                    //binding context to display object 
                    BindingContext = define;
                }
                catch
                {
                    UserDialogs.Instance.Toast("Could not find this word....", TimeSpan.MaxValue);
                }
            }
            else
            {
                UserDialogs.Instance.Toast("No Internet Access....", TimeSpan.MaxValue);
            }
        }
    }
    //classes to handle json object
    public partial class Dictionary
        {
            [JsonProperty("definitions")] public Definition[] Definitions { get; set; }

            [JsonProperty("word")] public string Word { get; set; }

            [JsonProperty("pronunciation")] public string Pronunciation { get; set; }

        }
        
        public class Definition
        {
            [JsonProperty("type")] public string Type { get; set; }

            [JsonProperty("definition")] public string Def { get; set; }

            [JsonProperty("example")] public string Example { get; set; }

            [JsonProperty("image_url")] public Uri ImageUrl { get; set; }

            [JsonProperty("emoji")] public object Emoji { get; set; }

        }
        //function to parse the json data into a Dictionary object
        public partial class Dictionary
        {
            public static Dictionary FromJson(string json) =>
                JsonConvert.DeserializeObject<Dictionary>(json, Converter.Settings);
        }


        //class to handle parsing metadata
        public class Converter
        {
            public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
                Converters =
                {
                    new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
                },
            };
        }

    
}
